 # Quelques astuces pour écrire en rmd (LaTeX) vers pdf 

- Ecrire en français
```
header-includes:
- \usepackage[french]{babel}
```

- Insérer une page blanche (utile si rédaction 1 page sur 2)

```
\newpage
\null
\thispagestyle{empty}
\newpage
```

- Changer le style de pagination 
```
\pagenumbering{arabic} 
\pagenumbering{roman} 
```

- Pagination 1 page sur 2

```
header-includes:
-\usepackage{fancyhdr}
---
[...]
\newcounter{mypage}
\newcommand{\Myfoot}{\stepcounter{mypage}\ifodd\value{mypage}
\thepage\else\addtocounter{page}{-1}\fi}
\fancyhf{}
\fancyfoot[CE]{\Myfoot}
\fancyfoot[CO]{\Myfoot}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\pagestyle{fancy}
```


- Ajouter annexes en fin de document  
Important notamment pour la pagination qui repart à 0 dans les annexes
```
fontsize: 11pt
output:
  pdf_document:
    fig_caption: yes
    fig_crop: no
    number_sections: yes
    toc: no
    includes:
      after_body: annexes.tex
```